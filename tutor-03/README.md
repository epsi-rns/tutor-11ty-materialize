
## Tutor 03

* Add Custom SASS (Custom Design)

* Apply Two Column Responsive Layout for Most Layout

* Nice Badge in Tags Page

![Eleventy Materialize: Tutor 03][11ty-materialize-preview-03]

[11ty-materialize-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-03/11ty-materialize-preview.png

