const moment = require("moment");

// Put main config above
// to avoid distraction from complex configuration
const config = {
    // URL related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "md",
      "njk",
      "html"
    ],

    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",

    // directory management
    passthroughFileCopy: true,
    dir: {
      input: "src",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };

module.exports = function(eleventyConfig) {

  // directory management
  eleventyConfig.addPassthroughCopy("assets");

  // layout alias
  eleventyConfig.addLayoutAlias("home",     "layouts/home.njk");
  eleventyConfig.addLayoutAlias("page",     "layouts/page.njk");
  eleventyConfig.addLayoutAlias("post",     "layouts/post.njk");
  eleventyConfig.addLayoutAlias("archives", "layouts/archives.njk");
  eleventyConfig.addLayoutAlias("tags",     "layouts/tags.njk");
  eleventyConfig.addLayoutAlias("tag-name", "layouts/tag-name.njk");
  
  // miscellanous filter
  eleventyConfig.addNunjucksFilter("date", function(date, format) {
    return moment(date).format(format);
  });

  // collection
  eleventyConfig.addCollection("tagList",
    require("./src/_11ty/getTagList"));

  // Return your Config object
  return config;
};
