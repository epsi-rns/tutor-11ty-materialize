# Eleventy Materialize Test Drive

An example of 11ty site using Materialize for personal learning purpose.

> 11ty + Nunjucks + Materialize

![Eleventy Materialize: Preview][11ty-materialize-preview]

-- -- --

## Links

### Demo Site

* <https://eleventy-step.netlify.com/>

### Eleventy Step By Step

> This repository:

* [Eleventy Step by Step Repository][tutorial-11ty]

### Eleventy Tutorial

A thorough Eleventy tutorial can be found in this article series:

* [Eleventy - Bulma MD - Overview][series-eleventy]

[tutorial-11ty]:        https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[series-eleventy]:      https://epsi-rns.gitlab.io/ssg/2020/01/01/11ty-bulma-md-overview/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Eleventy Configuration

* Layout: Base, Page, Post, Home, Archives, Index

* Layout: Tags List and Generate Each Tag Name (Using Pagination)

* Basic Content

![Eleventy Materialize: Tutor 01][11ty-materialize-preview-01]

-- -- --

### Tutor 02

* Add Materialize CSS

* Nice Header and Footer

* Enhance All Layouts with Materialize CSS

![Eleventy Materialize: Tutor 02][11ty-materialize-preview-02]

-- -- --

### Tutor 03

* Add Custom SASS (Custom Design)

* Apply Two Column Responsive Layout for Most Layout

* Nice Badge in Tags Page

![Eleventy Materialize: Tutor 03][11ty-materialize-preview-03]

-- -- --

### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Simplified All Layout Using Nunjucks Template Inheritance

* Configuration Stuff and Helper: Filter, Collection, Shortcodes

* Archives: Simple, By Year, List Tree (By Year and Month)

* Tags: List Tree

* Excerpt (<!--more-->): Read More Separator

* Custom Output: JSON

![Eleventy Materialize: Tutor 04][11ty-materialize-preview-04]

-- -- --

### Tutor 05

* Multi Column Responsive List: Tag, and Archive

* Widget: Friends, Archives Tree, Tags, Recent Post, Related Post

* Pagination: Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Eleventy Materialize: Tutor 05][11ty-materialize-preview-05]

-- -- --

### Tutor 06

> Finishing

* Layout: Service

* More Pagination: Archive

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes: pathPrefix, image

* Official Plugin: Syntax Highlight, RSS

* Meta: HTML, SEO, Opengraph, Twitter

![Eleventy Materialize: Tutor 06][11ty-materialize-preview-06]

-- -- --

## Additional Links

### Materialize Step By Step

* [Materialize Step by Step Repository][tutorial-materialize]

### HTML Step By Step

Additional guidance:

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md] (frankenbulma)

* [Bulma Step by Step Repository][tutorial-bulma]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui] (frankensemantic)

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/tutor-html-bulma-md/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

### Comparison

Comparation with other static site generator

* [Eleventy (Materialize) Step by Step Repository][tutorial-11ty-m]

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Hexo Step by Step Repository][tutorial-hexo]

* [Pelican Step by Step Repository][tutorial-pelican]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma-md/
[tutorial-11ty-m]:      https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

* [Eleventy - Presentation Slide][eleventy-presentation]

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css
[eleventy-presentation]:https://epsi-rns.gitlab.io/ssg/2020/02/07/11ty-presentation/

-- -- --

What do you think ?

[11ty-materialize-preview]:   https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/11ty-materialize-preview.png
[11ty-materialize-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-01/11ty-materialize-preview.png
[11ty-materialize-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-02/11ty-materialize-preview.png
[11ty-materialize-preview-03]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-03/11ty-materialize-preview.png
[11ty-materialize-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-04/11ty-materialize-preview.png
[11ty-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-05/11ty-materialize-preview.png
[11ty-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-06/11ty-materialize-preview.png

