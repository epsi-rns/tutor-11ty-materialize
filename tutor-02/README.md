
## Tutor 02

* Add Materialize CSS

* Nice Header and Footer

* Enhance All Layouts with Materialize CSS

![Eleventy Materialize: Tutor 02][11ty-materialize-preview-02]

[11ty-materialize-preview-02]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-02/11ty-materialize-preview.png

