---
layout    : post
title     : Marilyn Manson - Redeemer
date      : 2015-07-25 07:35:05
tags      : ["industrial metal", "90s"]

related_link_ids :
  - 15071535  # Sweet Dreams

---

Nothing seems exciting, always the same hiding
It's haunting me
<!--more-->

They say I cannot be this, 
I am jaded, hiding from the day
I can't bare, I cannot tame the hunger in me.
