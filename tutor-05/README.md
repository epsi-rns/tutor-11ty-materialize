
## Tutor 05

* Multi Column Responsive List: Tag, dan Archive.

* Widget: Friends, Archives Tree, Tags, Related Post, Related Post.

* Pagination: Adjacent, Indicator, Responsive.

* Post: Header, Footer, Navigation.

![Eleventy Materialize: Tutor 05][11ty-materialize-preview-05]

[11ty-materialize-preview-05]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-05/11ty-materialize-preview.png

