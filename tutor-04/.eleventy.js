// Put main config above
// to avoid distraction from complex configuration

const config = {
    // URL related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "md",
      "njk",
      "html"
    ],

    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",

    // directory management
    passthroughFileCopy: true,
    dir: {
      input: "src",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };

const moment = require("moment");

module.exports = function(eleventyConfig) {

  // miscellanous helper
  const helper = require("./src/_11ty/helper");

  // directory management
  eleventyConfig.addPassthroughCopy("assets");

  // layout alias
  eleventyConfig.addLayoutAlias("home",     "layouts/home.njk");
  eleventyConfig.addLayoutAlias("page",     "layouts/page.njk");
  eleventyConfig.addLayoutAlias("post",     "layouts/post.njk");
  eleventyConfig.addLayoutAlias("index",    "layouts/index.njk");
  eleventyConfig.addLayoutAlias("archives", "layouts/archives.njk");
  eleventyConfig.addLayoutAlias("tags",     "layouts/tags.njk");
  eleventyConfig.addLayoutAlias("tag-name", "layouts/tag-name.njk");

  // Miscellanous Filters
  
  // Copy paste from Jérôme Coupé
  eleventyConfig.addNunjucksFilter("date", function(date, format) {
    return moment(date).format(format);
  });

  // Custom: Grouping by date
  eleventyConfig.addNunjucksFilter("mapdate", function(posts) {
    return posts.map(post => ({ 
      ...post,
      year:      moment(post.date).format("Y"),
      month:     moment(post.date).format("MM"),
      monthtext: moment(post.date).format("MMMM")
    }));
  });

  // Copy paste from my other tutorial (Hexo)
  eleventyConfig.addNunjucksFilter("groupBy", function(posts, key) {
    return helper.groupBy(posts, key);
  });

  // Miscellanous Collection

  // Copy paste from Zach
  eleventyConfig.addCollection("tagList",
    require("./src/_11ty/getTagList"));

  // Filter using `Array.filter`
  eleventyConfig.addCollection("posts", function(collection) {
    return collection.getAll().filter(function(item) {
      // Filter by layout name
      return "post" === item.data.layout;
    });
  });

  // Miscellanous Shortcodes
  
  // Defines shortcode for generating post excerpts
  // Copy paste from Jérôme Coupé
  const excerpt = require("./src/_11ty/excerpt");
  eleventyConfig.addShortcode('excerpt',
    post => excerpt.extractExcerpt(post));

  // My own example custom module
  const test = require("./src/_11ty/simpleShortcode");
  eleventyConfig.addShortcode('exampleShortcode',
    post => test.showText(post));

  // Return your Config object
  return config;
};
