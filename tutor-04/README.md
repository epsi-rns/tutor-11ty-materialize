
## Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo.

* Simplified All Layout Using Nunjucks Template Inheritance

* Configuration Stuff and Helper: Filter, Collection, Shortcodes

* Archives: Simple, By Year, List Tree (By Year and Month)

* Tags: List Tree

* Excerpt (<!--more-->): Read More Separator

* Custom Output: JSON

![Eleventy Materialize: Tutor 04][11ty-materialize-preview-04]

[11ty-materialize-preview-04]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-04/11ty-materialize-preview.png

