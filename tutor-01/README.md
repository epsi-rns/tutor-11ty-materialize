
## Tutor 01

> Generate Only Pure HTML

* Eleventy Configuration

* Layout: Base, Page, Post, Home, Archives, Index

* Layout: Tags List and Generate Each Tag Name (Using Pagination)

* Basic Content

![Eleventy Materialize: Tutor 01][11ty-materialize-preview-01]

[11ty-materialize-preview-01]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-01/11ty-materialize-preview.png

