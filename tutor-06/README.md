
## Tutor 06

> Finishing

* Layout: Service

* More Pagination: Archive

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Post: Shortcodes: pathPrefix, image

* Official Plugin: Syntax Highlight, RSS

* Meta: HTML, SEO, Opengraph, Twitter

![Eleventy Materialize: Tutor 06][11ty-materialize-preview-06]

[11ty-materialize-preview-06]:https://gitlab.com/epsi-rns/tutor-11ty-materialize/raw/master/tutor-06/11ty-materialize-preview.png

